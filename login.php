<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
if (!empty($_SESSION['login']))
{
    header('Location: index.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if(!empty($_COOKIE['login_error']))
    {
      setcookie('login_error', '', 100000);
      print('Такого логина не существует.');
    }
    ?>

    <form action="login.php" method="post">
        <input name="login" placeholder="Логин" />
        <input name="pass" placeholder="Пароль" />
        <input type="submit" value="Войти" />
    </form>

    <?php
}
else
{
    $db = new PDO('mysql:host=localhost;dbname=u35658', 'u35658', '3463246', array(PDO::ATTR_PERSISTENT => true));
    try
    {
        $stmt = $db->prepare("SELECT `login`,`pass` FROM `users` WHERE login = ?");
        $stmt->execute(array($_POST['login']));
        $result = $stmt->fetchAll();
    }
    catch(PDOException $e)
    {
        print('Error : ' . $e->getMessage());
        exit();
    }
    if($_POST['pass'] == $result[0][1])
    {
        $stmt = $db->prepare("SELECT `id` FROM `users` WHERE login = ? AND pass = ?");
        $stmt->execute(array($_POST['login'], $result[0][1]));
        $result = $stmt->fetchAll();
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['sid'] = $result[0]["id"];    
        header('Location: index.php');
    }
    else
    {
        setcookie('login_error', '1', time() + 24*60*60);
        header('Location: login.php');
    }
}
